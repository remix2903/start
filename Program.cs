using System;

namespace String
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 1, 3, 9, 43 };
            int[] arr2 = {2, 5, 6, 8, 12, 42, 54};
            int[] merged = mergeArray(arr, arr2);

            foreach(int i in merged)
            {
                Console.Write(i + ",");
            }

        }

       static int[] mergeArray(int[] arr1, int[]arr2)
        {
            int leftPointer = 0;
            int rightPointer = 0;
            int index = 0;
            int[] result = new int[arr1.Length + arr2.Length];
            while(leftPointer < arr1.Length || rightPointer < arr2.Length)
            {
                while(leftPointer < arr1.Length && rightPointer < arr2.Length)
                {
                    if (arr1[leftPointer] <= arr2[rightPointer])
                    {
                        result[index++] = arr1[leftPointer++];
                    }
                    else
                    {
                        result[index++] = arr2[rightPointer++];
                    }
                }
                
                while(leftPointer < arr1.Length || rightPointer < arr2.Length)
                {
                    if(leftPointer < arr1.Length)
                    {
                        result[index++] = arr1[leftPointer++];
                    }
                    else
                    {
                        result[index++] = arr2[rightPointer++];
                    }
                    
                }
            }
            
            return result;
        }
    }
}
